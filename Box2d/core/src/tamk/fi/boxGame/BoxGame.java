package tamk.fi.boxGame;

import com.badlogic.gdx.*;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.actions.ParallelAction;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.Timer;
import tamk.fi.boxGame.Settings.Static;
import tamk.fi.boxGame.TimerTasks.GhostPowerTask;

import java.util.ArrayList;
import java.util.Iterator;

public class BoxGame extends Game implements ContactListener{


	SpriteBatch batch;

	// world stuff
	public static final float WORLD_WIDTH = 1536.0f / 100.0f;
	public static final float WORLD_HEIGHT = 800.0f / 100.0f;
	public static final float WINDOW_WIDTH = 1536.0f;
	public static final float WINDOW_HEIGHT = 800.0f;
	public static World world;

	public final static short GROUP_BONE = -1;

	// World Stepping variables.
	private double accumulator = 0;
	public double currentTime;

	// screen
	private GameScreen gameScreen;
	private GameOverScreen gameOverScreen;

	// objects
	private Player player;
	private ArrayList<Bone> boneList;
	private ArrayList<BoneDataHandler> bonesToBeAdded;

	// debug
	public static Box2DDebugRenderer debug;

	// score
	private int score;
	private int difficulty = 50;

	// time
	private long startTime;
	private long endTime   ;
	private long totalTime ;
	private Timer boneTimer;
	private Timer crawlTimer;
	private Timer ghostPowerTimer;

	// particle test
	private ArrayList<ParticleEffect> particleEffects;

	@Override
	public void create () {
		batch = new SpriteBatch();
		score = 0;

		// world stuff
		float gravityX  = 0f;
		float gravityY  = -9.81f;
		world = new World(new Vector2(gravityX, gravityY), true);
		world.setContactListener(this);

		// time step
		accumulator = 0.0f;
		currentTime = TimeUtils.millis() / 1000.0;

		// objects
		player = new Player(this ,WORLD_WIDTH /2,WORLD_HEIGHT /2); // player with spawn coordinates to middle
		debug = new Box2DDebugRenderer(); // debug renderer
		createGround();
		createWall();
		boneList = new ArrayList<Bone>();
		bonesToBeAdded = new ArrayList<BoneDataHandler>();

		// time
		startTime = System.currentTimeMillis();
		boneTimer = new Timer();
		crawlTimer = new Timer();
		ghostPowerTimer = new Timer();

		// screens
		gameScreen = new GameScreen(this, player);
		gameOverScreen = new GameOverScreen(this);
		setScreen(gameOverScreen);

		// testing part
		particleEffects = new ArrayList<ParticleEffect>();
	}

	@Override
	public void render () {
		super.render();
	}

	// --- methods ---

	@Override
	public void beginContact(Contact contact) {

		Object dataA;
		Object dataB;

		if(contact.getFixtureA().getUserData() == null){
			 dataA = contact.getFixtureA().getBody().getUserData();
		}
		else{
			dataA = contact.getFixtureA().getUserData();
		}
		if(contact.getFixtureB().getUserData() == null){
			dataB = contact.getFixtureB().getBody().getUserData();
		}
		else{
			dataB = contact.getFixtureB().getUserData();
		}
		// bones
		if(dataA instanceof Bone && dataB.equals("ground")|| dataB instanceof  Bone && dataA.equals("ground")){
			System.out.println("Bone collision with ground");
			boneToGround(dataA, dataB);
		}
		else if(dataA instanceof Bone && dataB.equals("wall")|| dataB instanceof  Bone && dataA.equals("wall")){
			System.out.println("Bone collision with wall");
			boneToWall(dataA, dataB);
		}

		// player
		else if(dataA.equals("playerFootSensor") && dataB.equals("ground") || dataA.equals("ground") && dataB.equals("playerFootSensor")){
			System.out.println("Player collision with ground");
			player.land();
		}
		else if(dataA instanceof Bone && dataB.equals("playerBodySensor")|| dataB instanceof  Bone && dataA.equals("playerBodySensor")){
			System.out.println("Player collision with bone");
			playerToBone(dataA, dataB);
		}


	}

	@Override
	public void endContact(Contact contact) {
	}
	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
	}
	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
	}

	// map, render, input
	public void doPhysicsStep() {
		// We are not using Gdx.graphics.getDeltaTime(), it may not
		// be accurate enough.

		// Current time
		double newTime = TimeUtils.millis() / 1000.0;

		// How much time since last call?
		double deltaTime = newTime - currentTime;
		currentTime = newTime;

		float TIME_STEP = 1/60f;

		// Which one is faster, frameTime or 0.25?
		if(deltaTime > 0.25) {
			deltaTime = 0.25;
		}

		accumulator += deltaTime;
		while (accumulator >= TIME_STEP) {
			world.step(TIME_STEP, 6, 2);
			accumulator -= TIME_STEP;
		}

	}
	public void gameRender(SpriteBatch batchFrom, float delta){

		player.functionality();

		addNewBonesFromAddList();
		checkBoneRemoval();
		renderBones();
		for(int n = 0; n < particleEffects.size(); n++){
			if(particleEffects.get(n).isComplete()){
				particleEffects.get(n).dispose();
				particleEffects.remove(n);
			}
			else{
				particleEffects.get(n).draw(batchFrom);
				particleEffects.get(n).update(delta);
			}
		}

		endTime   = System.currentTimeMillis();
		totalTime = endTime - startTime;

	}
	public void createGround() {
		// *** 1) DEFINITION OF A BODY *** /

		// Body Definition
		BodyDef myBodyDef = new BodyDef();

		// This body won't move
		myBodyDef.type = BodyDef.BodyType.StaticBody;

		// Initial position is centered up
		// This position is the CENTER of the shape!
		myBodyDef.position.set(WORLD_WIDTH / 2, 0.15f);

		// *** 2) CREATE THE BODY *** /
		Body groundBody = world.createBody(myBodyDef);

		// *** 3) FIXTURE FOR THE BODY *** /

		// Create shape
		PolygonShape groundBox = new PolygonShape();

		// Real width and height is 2 X this!
		groundBox.setAsBox( WORLD_WIDTH/2 , 0.15f);

		// Add shape to fixture, 0.0f is density.
		// Using method createFixture(Shape, density) no need
		// to create FixtureDef object as on createPlayer!
		groundBody.createFixture(groundBox, 0.0f);

		groundBody.setUserData("ground");
	}
	public void createWall() {
		BodyDef myBodyDef = new BodyDef();
		myBodyDef.type = BodyDef.BodyType.StaticBody;
		myBodyDef.position.set(WORLD_WIDTH + 0.5f, WORLD_HEIGHT/2);

		// *** 2) CREATE THE BODY *** /
		Body groundBody = world.createBody(myBodyDef);

		// Create shape
		PolygonShape groundBox = new PolygonShape();

		// Real width and height is 2 X this!
		groundBox.setAsBox( 0.5f , WORLD_HEIGHT / 2);

		// Add shape to fixture, 0.0f is density.
		// Using method createFixture(Shape, density) no need
		// to create FixtureDef object as on createPlayer!
		groundBody.createFixture(groundBox, 0.0f);
		groundBody.setUserData("wall");



		BodyDef myBodyDef2 = new BodyDef();
		myBodyDef2.type = BodyDef.BodyType.StaticBody;
		myBodyDef2.position.set(-0.5f, WORLD_HEIGHT/2);

		// *** 2) CREATE THE BODY *** /
		Body leftWall = world.createBody(myBodyDef2);

		// Create shape
		PolygonShape leftWallShape = new PolygonShape();

		// Real width and height is 2 X this!
		leftWallShape.setAsBox( 0.5f , WORLD_HEIGHT / 2);

		// Add shape to fixture, 0.0f is density.
		// Using method createFixture(Shape, density) no need
		// to create FixtureDef object as on createPlayer!
		leftWall.createFixture(leftWallShape, 0.0f);
		leftWall.setUserData("wall");

	}
	public void getUserInput() {

		if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE))  {
			if(!gameScreen.getDebug()){
				gameScreen.setDebug(true);
			}
			else {
				gameScreen.setDebug(false);
			}
		}

		if (Gdx.input.isKeyPressed(Input.Keys.DPAD_LEFT))  {
			player.moveLeft();
		}
		if (Gdx.input.isKeyPressed(Input.Keys.DPAD_RIGHT)) {
			player.moveRight();
		}
		if (Gdx.input.isKeyJustPressed(Input.Keys.DPAD_UP)) {
			player.jump();
		}
		if (Gdx.input.isKeyJustPressed(Input.Keys.DPAD_DOWN)) {
			if(!player.isExhausted()){
				System.out.println("Ghost active!");
				ghostPowerTimer.scheduleTask(new GhostPowerTask(player), Static.ghostDuration);
			}
		}
	}

	// utility
	public int randomWithRange(int min, int max)
	{
		int range = (max - min) + 1;
		return (int)(Math.random() * range) + min;
	}

	// bone stuff
	public void spawnBoneTask(){

		if(this.getScreen() instanceof GameScreen){ // only spawn during game
			int difficultyIncrease = (score/5);
			int randomBone = randomWithRange(1,100) + difficultyIncrease;

			if(randomBone <= 75){
				boneList.add(new NormalBone(this,randomWithRange(0, (int)WORLD_WIDTH), WORLD_HEIGHT,0,0));
				System.out.println("Spawning normal bone");
			}
			else if(randomBone < 95 && randomBone > 75){
				boneList.add(new BouncyBone(this,randomWithRange(0, (int)WORLD_WIDTH), WORLD_HEIGHT,0,0));
				System.out.println("Spawning bouncy bone");
			}
			else if(randomBone < 105 && randomBone >= 95) {
				System.out.println("Spawning giant bone");
				boneList.add(new GiantBone(this,randomWithRange(0, (int)WORLD_WIDTH), WORLD_HEIGHT,0,0));
			}
			else if(randomBone < 125 && randomBone >= 105){
				boneList.add(new Ribcage(this, randomWithRange(0, (int) WORLD_WIDTH), WORLD_HEIGHT,0,0));
				System.out.println("Spawning ribcage");
			}
			else if(randomBone < 140 && randomBone >= 125){
				System.out.println("Spawning crawling hand");
				boneList.add(new CrawlingHand(this,randomWithRange(0, (int)WORLD_WIDTH), WORLD_HEIGHT,0,0));
			}
			else if(randomBone > 140){
				System.out.println("Spawning dark bone");
				boneList.add(new DarkBone(this,randomWithRange(0, (int)WORLD_WIDTH), WORLD_HEIGHT,0,0));
			}

		}

	}
	private void renderBones(){
		for(Bone bone : boneList){
			bone.drawBone();
		}
	}
	private void removeBones(){
		// remove all bones from the bone list
		Iterator<Bone> itr = boneList.iterator();

		while (itr.hasNext()) {
			Bone bone = itr.next();
			world.destroyBody(bone.getBoneBody());
			itr.remove();
		}

	}
	private void checkBoneRemoval() {

		// remove collided normal bones
		Iterator<Bone> itr = boneList.iterator();
		while (itr.hasNext()) {
			Bone bone = itr.next();

			if (((bone instanceof NormalBone ||
					bone instanceof Ribcage ||
					bone instanceof CrawlingHand) ||
					bone instanceof GiantBone)
					&& bone.getCollidedStatus()) { //Bones destroyed due to collided status: normal bones, rib cages, Crawling hands, giant

				addBoneParticles(bone);
				world.destroyBody(bone.getBoneBody());
				itr.remove();
				score++;

			}

			if ((bone instanceof BouncyBone||
					bone instanceof DarkBone)
			&& (bone.getCollidedStatus() || !bone.getBoneBody().isAwake())) { // bouncy ones, dark ones

				addBoneParticles(bone);
				world.destroyBody(bone.getBoneBody());
				itr.remove();
				score++;
			}

		}
	}
	private void addNewBonesFromAddList(){

		for(BoneDataHandler bone : bonesToBeAdded){

				if(!bone.isSpawnedBy()){

					if(bone.getBoneType() == 1) {
						boneList.add(new NormalBone(this,bone.getSpawnX(),bone.getSpawnY(),0,0));
					}
					else if(bone.getBoneType() == 2) {
						boneList.add(new BouncyBone(this,bone.getSpawnX(),bone.getSpawnY(),0,0));
					}
					else if(bone.getBoneType() == 3) {
						boneList.add(new Ribcage(this,bone.getSpawnX(), bone.getSpawnY(),0,0));
					}
				}
				else{
					spawnedBone(bone);
				}
		}
		bonesToBeAdded.clear();
	}
	private void spawnedBone(BoneDataHandler bone){

		float helpX = 0;
		float helpY = 0;

		float helpSpawn;

		if(bone.getSpawnID() == 0){ // left
			helpX = randomWithRange(-35,-20) * 0.1f;
			helpY = (randomWithRange(20,50) * 0.2f)/2;
		}
		else if(bone.getSpawnID() == 1){ // middle
			helpX = randomWithRange(-10,10) * 0.1f;
			helpY = randomWithRange(20,35) * 0.2f;
		}
		else if(bone.getSpawnID() == 2){ // right
			helpX = randomWithRange(20,35) * 0.1f;
			helpY = (randomWithRange(20,50) * 0.2f)/2;
		}

		if(bone.getBoneType() == 1) {
			boneList.add(new NormalBone(this,bone.getSpawnX(),bone.getSpawnY(),helpX,helpY));
		}
		else if(bone.getBoneType() == 2) {
			boneList.add(new BouncyBone(this,bone.getSpawnX(),bone.getSpawnY(),helpX,helpY));
		}
		else if(bone.getBoneType() == 3) {
			boneList.add(new Ribcage(this,bone.getSpawnX(),bone.getSpawnY(),helpX,helpY));
		}


	}
	public void crawlingHandTask(){

		Iterator<Bone> itr = boneList.iterator();

		while (itr.hasNext()) {
			Bone bone = itr.next();
			if(bone instanceof CrawlingHand){
				((CrawlingHand) bone).crawl();
			}
		}
	}
	private void addBoneParticles(Bone bone){
		Vector2 bonePos = bone.getBoneBody().getPosition();
		ParticleEffect effect = loadEffect();
		effect.setPosition(bonePos.x,bonePos.y/2) ;
		effect.start();
		particleEffects.add(effect);
	}

	// settings
	private void resetData(){ // also for when entering game, initializing values

		removeBones(); // iterate and remove all bodies from world and all bones from map
		score = 0; // reset score for title screen "You survived x bones"

		player.getBody().setLinearVelocity(0,0); // remove player speed
		player.getBody().setTransform(WORLD_WIDTH /2,WORLD_HEIGHT /2, 0); // set player to middle

		startTime = System.currentTimeMillis(); // reset time for how long game has been running
		boneTimer.clear(); // restart timer for bone spawns
		boneTimer.scheduleTask(new SpawnBonesTask(this), 1f, 5.0f - (difficulty * 0.045f));
		crawlTimer.clear();
		crawlTimer.scheduleTask(new crawlingHandsMovementTask(this), 1f, 0.1f);
		ghostPowerTimer.clear();
		System.out.println("Difficulty set as: " + (5.0f - (difficulty * 0.045f)) +" seconds between bone spawns.");

		player.setHealth(3); // reset health value

	}
	public void setScreenGame(){

		resetData();
		this.setScreen(gameScreen);

	}
	public void setDifficulty(int oneToHundred){
		if(oneToHundred < 1){
			difficulty = 100;
		}
		else if(oneToHundred > 100){
			difficulty = 1;
		}
		else{
			difficulty = oneToHundred;
		}

	}
	public  int getDifficulty(){
		return difficulty;
	}
	public int getScore(){
		return score;
	}
	public ArrayList<BoneDataHandler> getBoneList(){
		return bonesToBeAdded;
	}
	public Vector2 getPlayerPosition(){
		return player.getBody().getPosition();
	}

	public ParticleEffect loadEffect(){
		ParticleEffect effect = new ParticleEffect();
		effect.load(Gdx.files.internal("explo test.p"),Gdx.files.internal(""));
		return effect;
	}

	// collisions
	private void playerToBone(Object a, Object b){
		player.setHealth(player.getHealth()-1);

		Body helpBody;

		if(a instanceof Bone){
			helpBody = ((Bone) a).getBoneBody();
		}
		else{
			helpBody = ((Bone) b).getBoneBody();
		}

		Iterator<Bone> itr = boneList.iterator();

		while (itr.hasNext() && helpBody != null) {

			Bone bone = itr.next();

			if(bone.getBoneBody().equals(helpBody)){
				bone.collided();
			}
		}
			if (player.getHealth() < 1) {
				this.setScreen(gameOverScreen);
			}

	}
	private void boneToGround(Object a, Object b){

		Body helpBody;

		if(a instanceof Bone){
			helpBody = ((Bone) a).getBoneBody();
		}
		else{
			helpBody = ((Bone) b).getBoneBody();
		}

		Iterator<Bone> itr = boneList.iterator();

		while (itr.hasNext() && helpBody != null) {

			Bone bone = itr.next();

			if((bone instanceof NormalBone || bone instanceof GiantBone) && bone.getBoneBody().equals(helpBody)){
				bone.collided();
			}
			else if(bone instanceof Ribcage && bone.getBoneBody().equals(helpBody)){
				((Ribcage) bone).explode();
				bone.collided();
			}
			else if(bone instanceof CrawlingHand && bone.getBoneBody().equals(helpBody)){
				((CrawlingHand) bone).setOnGround(true);
			}
		}
	}
	private void boneToWall(Object a, Object b){

		Body helpBody;

		if(a instanceof Bone){
			helpBody = ((Bone) a).getBoneBody();
		}
		else{
			helpBody = ((Bone) b).getBoneBody();
		}

		Iterator<Bone> itr = boneList.iterator();

		while (itr.hasNext() && helpBody != null) {

			Bone bone = itr.next();

			if(bone instanceof CrawlingHand && bone.getBoneBody().equals(helpBody)){
				bone.collided();
			}

		}
	}

	// get


	public GameOverScreen getGameOverScreen() {
		return gameOverScreen;
	}
}

