package tamk.fi.boxGame;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;


/**
 * Created by Natura on 15.2.2015.
 */
public class GameOverScreen implements Screen {

    // For debugging purposes
    public static final String TAG = "GameOverScreen";

    // Basic utilities
    private BoxGame game;
    private SpriteBatch batch;

    // camera stuff
    private OrthographicCamera staticCamera;

    // texture
    private Texture backgroundTexture;

    // FONT
    final int padding = 20;
    private BitmapFont defaultFont;

    private float survivedX;
    private float survivedY;

    private float diffultyX;
    private float diffultyY;

    private String survived;
    private String difficulty;

    // UI stuff
    boolean unlockSlider = true;


    public GameOverScreen(BoxGame g) {
        game = g;
        batch = game.batch;

        staticCamera = new OrthographicCamera();
        staticCamera.setToOrtho(false, game.WINDOW_WIDTH, game.WINDOW_HEIGHT);

        backgroundTexture = new Texture("spookyMenu.jpg");
        defaultFont = new BitmapFont(Gdx.files.internal("font.txt"));

        survived = "You survived " + Integer.toString(game.getScore()) + " bones!";
        difficulty = String.valueOf(game.getDifficulty());

        survivedX = (game.WINDOW_WIDTH + defaultFont.getBounds("sd").width) /1.82f;
        diffultyX = (game.WINDOW_WIDTH + defaultFont.getBounds("sd").width) /5;

        survivedY = game.WINDOW_HEIGHT * 0.3f;
        diffultyY = game.WINDOW_HEIGHT * 0.7f;

    }


    @Override
    public void render(float delta) {

        if(game.getScore() > 1){
            survived = "You survived " + Integer.toString(game.getScore()) + " bones!";
        }
        else if(game.getScore() == 0){
            survived = "";
        }
        else{
            survived = "You survived " + Integer.toString(game.getScore()) + " bone! Hurray!";
        }

        difficulty = String.valueOf(game.getDifficulty());

        //static cam
        batch.setProjectionMatrix(staticCamera.combined);

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
        batch.draw(backgroundTexture, 0,0, game.WINDOW_WIDTH, game.WINDOW_HEIGHT);

        defaultFont.setScale(0.6f);
        defaultFont.draw(batch, survived, survivedX, survivedY);
        defaultFont.draw(batch, difficulty, diffultyX, diffultyY);


        batch.end();


        if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER)) {
            game.setScreenGame();
            unlockSlider = false;
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.DPAD_LEFT) || Gdx.input.isKeyJustPressed(Input.Keys.DPAD_RIGHT)){
            unlockSlider = true;
        }

        if (Gdx.input.isKeyPressed(Input.Keys.DPAD_LEFT) && unlockSlider) {
            game.setDifficulty(game.getDifficulty() -1);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.DPAD_RIGHT) && unlockSlider) {
            game.setDifficulty(game.getDifficulty() +1);
        }

    }

    @Override
    public void show() {
        Gdx.app.log(TAG, "show()");

    }

    @Override
    public void resize(int width, int height) {
        Gdx.app.log(TAG, "resize width = " + width + ", height = " + height);

    }

    @Override
    public void pause() {
        Gdx.app.log(TAG, "pause()");

    }

    @Override
    public void resume() {
        Gdx.app.log(TAG, "resume()");

    }

    @Override
    public void hide() {
        Gdx.app.log(TAG, "hide()");

    }

    @Override
    public void dispose() {
        Gdx.app.log(TAG, "dispose()");

    }

    public BitmapFont getDefaultFont() {
        return defaultFont;
    }
}
