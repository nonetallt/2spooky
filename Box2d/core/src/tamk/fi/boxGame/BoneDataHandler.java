package tamk.fi.boxGame;

/**
 * Created by Natura on 3.3.2015.
 */
public class BoneDataHandler {

    private float spawnX;
    private float spawnY;
    private int boneType;
    private boolean spawnedBy;
    private int spawnID;

    public BoneDataHandler(float x, float y, int bone, boolean spawned, int spawnNumber){

        spawnX = x;
        spawnY = y;
        boneType = bone;
        spawnedBy = spawned;

        if(spawnedBy){
            spawnID = spawnNumber;
        }
        else{
            spawnID = -1;
        }
    }

    public float getSpawnX() {
        return spawnX;
    }

    public float getSpawnY() {
        return spawnY;
    }

    public int getBoneType() {
        return boneType;
    }

    public boolean isSpawnedBy() {
        return spawnedBy;
    }

    public int getSpawnID() {
        return spawnID;
    }
}
