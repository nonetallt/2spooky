package tamk.fi.boxGame;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by Natura on 15.2.2015.
 */
public class GameScreen implements Screen {

    // For debugging purposes
    public static final String TAG = "GameScreen";

    // basic utility
    private BoxGame game;
    private SpriteBatch batch;

    // camera stuff
    private OrthographicCamera staticCamera;
    private OrthographicCamera statusCamera;

    private boolean debug = false;

    // background
    private Texture backgroundTexture;

    // info
    Player player;




    public GameScreen(BoxGame g, Player p) {
        game = g;
        player = p;
        batch = game.batch;

        staticCamera = new OrthographicCamera();
        staticCamera.setToOrtho(false, game.WORLD_WIDTH, game.WORLD_HEIGHT);

        statusCamera = new OrthographicCamera();
        statusCamera.setToOrtho(false, game.WINDOW_WIDTH, game.WINDOW_HEIGHT);

        backgroundTexture = new Texture("spookyBackground.jpg");
    }



    @Override
    public void render(float delta) {

        //static cam
        batch.setProjectionMatrix(staticCamera.combined);

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
        if(debug){
            game.debug.render(game.world, staticCamera.combined);

        }
        else{
            batch.draw(backgroundTexture, 0,0, game.WORLD_WIDTH, game.WORLD_HEIGHT);
            game.gameRender(batch,delta);
            batch.setProjectionMatrix(statusCamera.combined);
            statusRender();
        }
        batch.end();
        game.getUserInput();
        game.doPhysicsStep();
    }
    private void statusRender(){
        // draw health
        player.drawHealth();
        // draw score and difficulty
        int difficultyIncrease = (game.getScore()/5);
        String text = "Score: " + game.getScore() + "   [D+ " + difficultyIncrease +  "]";
        float survivedX =  ((game.WINDOW_WIDTH + game.getGameOverScreen().getDefaultFont().getBounds(text).width) / 2 - (game.getGameOverScreen().getDefaultFont().getBounds(text).width)) ;
        float  survivedY = (game.WINDOW_HEIGHT) * 0.95f;
        game.getGameOverScreen().getDefaultFont().draw(batch, text, survivedX, survivedY);
    }

    @Override
    public void show() {
        Gdx.app.log(TAG, "show()");

    }

    @Override
    public void resize(int width, int height) {
        Gdx.app.log(TAG, "resize width = " + width + ", height = " + height);

    }

    @Override
    public void pause() {
        Gdx.app.log(TAG, "pause()");

    }

    @Override
    public void resume() {
        Gdx.app.log(TAG, "resume()");

    }

    @Override
    public void hide() {
        Gdx.app.log(TAG, "hide()");

    }

    @Override
    public void dispose() {
        Gdx.app.log(TAG, "dispose()");

    }

    public void setDebug(boolean doDebug){
        debug = doDebug;
    }
    public boolean getDebug(){
        return  debug;
    }

}
