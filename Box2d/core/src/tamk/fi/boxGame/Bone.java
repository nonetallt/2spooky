package tamk.fi.boxGame;

import com.badlogic.gdx.physics.box2d.Body;

/**
 * Created by Natura on 3.3.2015.
 */
abstract public class Bone {

    abstract public void drawBone();
    abstract public Body getBoneBody();
    abstract public void collided();
    abstract public boolean getCollidedStatus();
}
