package tamk.fi.boxGame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

/**
 * Created by Natura on 21.2.2015.
 */
public class Player {

    // world
    private BoxGame game;
    private float startingX;
    private float startingY;
    private SpriteBatch batch;

    // player
    private float width;
    private float height;
    private float jumpHeight = 7.0f;
    private float currentSpeed = 0;
    private float acceleration = 0.2f;
    private float airAcceleration = 0.025f;
    private float maximumVelocity = 2.5f;
    private float airMaximumVelocity = 2.5f;

    // box2d
    private BodyDef myBodyDef;
    private Body playerBody;
    private PolygonShape groundBox;
    private PolygonShape sensorBox;

    // mechanics
    private boolean movingRight = false;
    private boolean onAir = false;
    private int health = 3;
    private Texture playerHealthTexture;
    private boolean ghosting = false;
    private boolean exhausted = false;
    private Fixture playerBodyFixture;

    // animation
    private float stateTime = 0.0f;

    private Animation currentAnimation;
    private TextureRegion currentFrame;
    private Texture idleSheet;
    private Texture moveSheet;
    private Texture moveSheetFlip;
    private Texture jumpSheet;
    private Animation playerAnimationIdle;
    private Animation playerAnimationMove;
    private Animation playerAnimationMoveFlip;
    private Animation playerAnimationJump;

    TextureRegion[][] tmp1;


    public Player(BoxGame g, float x, float y){

        game = g;
        startingX = x;
        startingY = y;
        batch = game.batch;

        idleSheet = new Texture("2spookyIdle.png");
        moveSheet = new Texture("2spookyMove.png");
        jumpSheet = new Texture("2spookyJump.png");
        moveSheetFlip = new Texture("2spookyMoveFlip.png");
        playerHealthTexture = new Texture("bone.png");

        width = idleSheet.getWidth() / 6 /100f ;
        height = idleSheet.getHeight()   / 100f ;

        create();

    }
    private void create(){

        // definition
        myBodyDef = new BodyDef();
        myBodyDef.fixedRotation = true;
        myBodyDef.type = BodyDef.BodyType.DynamicBody;
        myBodyDef.position.set(startingX, startingY);

        // *** 2) CREATE THE BODY *** /
        playerBody = game.world.createBody(myBodyDef);

        // *** 3) FIXTURE FOR THE BODY *** /

        //  main
        groundBox = new PolygonShape();
        groundBox.setAsBox(width, height);

        sensorBox = new PolygonShape();
        sensorBox.setAsBox(width * 0.9f, height / 10f ,new Vector2(0f,-0.6f), 0);


        width = width * 2;
        height = height * 2;

        FixtureDef playerFixtureDef = new FixtureDef();
        playerFixtureDef = new FixtureDef();
        playerFixtureDef.density = 5;
        playerFixtureDef.restitution = 0.0f;
        playerFixtureDef.friction = 0.5f;
        playerFixtureDef.filter.groupIndex = 1;

        playerFixtureDef.shape = groundBox;
        playerBodyFixture = playerBody.createFixture(playerFixtureDef);
        playerBodyFixture.setUserData("playerBodySensor");

        // sensor
        FixtureDef sensorFixtureDef = new FixtureDef();
        sensorFixtureDef.isSensor = true;
        sensorFixtureDef.shape = sensorBox;
        Fixture footSensorFixture = playerBody.createFixture(sensorFixtureDef);
        footSensorFixture.setUserData("playerFootSensor");

        // Add fixture to the body
        playerBody.createFixture(playerFixtureDef);
        playerBody.createFixture(sensorFixtureDef);

        playerBody.setUserData(this);


        // ---------------------------------

        // ANIMATION STUFF


        TextureRegion[][] tmp1 = TextureRegion.split(idleSheet, idleSheet.getWidth()/6, idleSheet.getHeight());
        TextureRegion[][] tmp2 = TextureRegion.split(moveSheet, moveSheet.getWidth()/3, moveSheet.getHeight());
        TextureRegion[][] tmp3 = TextureRegion.split(jumpSheet, jumpSheet.getWidth()/6, jumpSheet.getHeight());
        TextureRegion[][] tmp4 = TextureRegion.split(moveSheetFlip, moveSheetFlip.getWidth()/3, moveSheetFlip.getHeight());

        TextureRegion [] frames1 = transformTo1D(tmp1, 6, 1);
        TextureRegion [] frames2 = transformTo1D(tmp2, 3, 1);
        TextureRegion [] frames3 = transformTo1D(tmp3, 6, 1);
        TextureRegion [] frames4 = transformTo1D(tmp4, 3, 1);

        playerAnimationIdle = new Animation(1 / 2f, frames1);
        playerAnimationMove = new Animation(1 / 10f, frames2);
        playerAnimationJump = new Animation(1 / 7.5f, frames3);
        playerAnimationMoveFlip = new Animation(1 / 10f, frames4);

        currentAnimation = playerAnimationIdle;
        // ---------------------------------

}

    private void drawPlayer(){

        stateTime += Gdx.graphics.getDeltaTime();

        if(currentAnimation == playerAnimationJump){
            currentFrame = currentAnimation.getKeyFrame(stateTime, false);

            if(currentAnimation.isAnimationFinished(stateTime)){
                setCurrentAnimation(1);
            }
        }
        else{
            currentFrame = currentAnimation.getKeyFrame(stateTime, true);
        }


        int animationHelp = 0;
        int frames = currentAnimation.getKeyFrames().length;


        for(int n = 0; n < currentAnimation.getKeyFrames().length; n++){

            if(currentFrame == currentAnimation.getKeyFrames()[n]){
                animationHelp = n;

            }
        }

        Color color = batch.getColor();
        if(ghosting){
            batch.setColor(color.r -0.15f,color.b,color.g -0.15f,0.6f);
        }
        else {
            batch.setColor(color.r,color.b,color.g,1.0f);
        }

        batch.draw(currentFrame.getTexture(),


                playerBody.getPosition().x - width / 2,
                playerBody.getPosition().y - height / 2,
                width / 2  ,                  // originX
                height / 2,                   // originY
                width,                        // width
                height,                       // height
                1.0f,                         // scaleX
                1.0f,                         // scaleY
                playerBody.getTransform().getRotation() * MathUtils.radiansToDegrees,
                animationHelp * currentFrame.getTexture().getWidth() / frames,          // Start drawing from x = 0
                0,                                                                      // Start drawing from y = 0
                currentFrame.getTexture().getWidth() / frames,                          // End drawing x
                currentFrame.getTexture().getHeight(),                                  // End drawing y
                false,                                                                  // flipX
                false);                                                                 // flipY
        batch.setColor(color.r,color.b,color.g,1.0f);
    }
    public Body getBody(){
        return playerBody;
    }

    public void moveLeft(){

        if(!onAir){


            if( movingRight){
                currentSpeed = 0;
                movingRight = false;

            }

            if(currentSpeed <= maximumVelocity){
                currentSpeed = currentSpeed + acceleration;
            }
            else{
                currentSpeed = maximumVelocity;
            }

            playerBody.applyLinearImpulse(new Vector2(-currentSpeed, 0f),
                    playerBody.getWorldCenter(),
                    true);
        }
        else{
            if( movingRight){
                currentSpeed = 0;
                movingRight = false;

            }

            if(currentSpeed <= airMaximumVelocity){
                currentSpeed = currentSpeed + airAcceleration;
            }
            else{
                currentSpeed = airMaximumVelocity;
            }

            playerBody.applyLinearImpulse(new Vector2(-currentSpeed, 0f),
                    playerBody.getWorldCenter(),
                    true);
        }

    }
    public void moveRight(){

        if(!onAir){
            if(!movingRight){
                currentSpeed = 0;
                movingRight = true;

            }

            if(currentSpeed <= maximumVelocity){
                currentSpeed = currentSpeed + acceleration;
            }
            else{
                currentSpeed = maximumVelocity;
            }
            playerBody.applyLinearImpulse(new Vector2(currentSpeed, 0f),
                    playerBody.getWorldCenter(),
                    true);
        }
        else{
            if(!movingRight){
                currentSpeed = 0;
                movingRight = true;
            }

            if(currentSpeed <= airMaximumVelocity){
                currentSpeed = currentSpeed + airAcceleration;
            }
            else{
                currentSpeed = airMaximumVelocity;
            }
            playerBody.applyLinearImpulse(new Vector2(currentSpeed, 0f),
                    playerBody.getWorldCenter(),
                    true);
        }


    }
    public void jump(){

        if(!onAir){

            setCurrentAnimation(3);
            onAir = true;

            float impulse = playerBody.getMass() * jumpHeight;

            playerBody.applyLinearImpulse(new Vector2(0f, impulse),
                    playerBody.getWorldCenter(),
                    true);
        }

    }
    public void land(){
        onAir = false;
    }
    public void setGhosting(boolean ghosting){
        this.ghosting = ghosting;
        if(ghosting){
            Filter filter = new Filter();
            filter.groupIndex = game.GROUP_BONE;
            playerBodyFixture.setFilterData(filter);
            playerBody.getFixtureList().clear();
            playerBody.getFixtureList().add(playerBodyFixture);
        }
        else{
            Filter filter = new Filter();
            filter.groupIndex = 1;
            playerBodyFixture.setFilterData(filter);
            playerBody.getFixtureList().clear();
            playerBody.getFixtureList().add(playerBodyFixture);
        }
    }

    private TextureRegion[] transformTo1D(TextureRegion[][] temp, int FRAME_COLS, int FRAME_ROWS){

        TextureRegion [] frames = new TextureRegion[FRAME_COLS * FRAME_ROWS];
        int index = 0;

        for(int n = 0; n < FRAME_ROWS; n++){
            for(int m = 0; m <  FRAME_COLS; m++){
                frames[index++] = temp[n][m];
            }
        }

        return frames;
    }
    public void setCurrentAnimation(int current){
        if(current == 1){
            currentAnimation = playerAnimationIdle;
        }
        if(current == 2){
            currentAnimation = playerAnimationMove;
        }
        if(current == 3){
            stateTime = 0;
            currentAnimation = playerAnimationJump;
        }
        if(current == 4){
            currentAnimation = playerAnimationMoveFlip;
        }
    }
    private void checkAnimationState(){

        Vector2 help = new Vector2();
        help = playerBody.getLinearVelocity();
        float x = help.x;

        if((x > 0.5f || x < -0.5f)&& !onAir){
            if(movingRight){
                setCurrentAnimation(2);
            }
            else{
                setCurrentAnimation(4);
            }

        }
        else if(!onAir){
            setCurrentAnimation(1);
        }
    }
    public void functionality(){
        checkAnimationState();
        drawPlayer();
    }
    public void setHealth(int newHealth){
        health = newHealth;
    }
    public int getHealth(){
        return health;
    }
    public void drawHealth(){

        float displayX = playerHealthTexture.getWidth();
        float displayY = playerHealthTexture.getHeight();

        for(int n = 0; n < health; n++){
            batch.draw(playerHealthTexture, displayX*3 + displayX*n*2, game.WINDOW_HEIGHT - displayY*2);
        }
    }

    public boolean isGhosting() {
        return ghosting;
    }

    public boolean isExhausted() {
        return exhausted;
    }

    public void setExhausted(boolean exhausted) {
        this.exhausted = exhausted;
    }
}
