package tamk.fi.boxGame;


import com.badlogic.gdx.utils.Timer;

/**
 * Created by Natura on 2.3.2015.
 */
public class SpawnBonesTask extends Timer.Task {

    private BoxGame game;

    public SpawnBonesTask(BoxGame g){
        game = g;
    }
    public void run(){
        game.spawnBoneTask();
    }
}
