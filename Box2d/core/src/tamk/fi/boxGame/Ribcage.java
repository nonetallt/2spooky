package tamk.fi.boxGame;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

/**
 * Created by Natura on 3.3.2015.
 */
public class Ribcage extends Bone {

    // utility
    private BoxGame game;
    private SpriteBatch batch;


    // starting
    private float startingX;
    private float startingY;
    private float scaleRandom;

    // texture
    private Texture boneTexture;

    // body
    private BodyDef myBodyDef;
    private Body normalBoneBody;
    private PolygonShape groundBox;

    private float width;
    private float height;

    // collided with player
    private boolean colldided = false;

    // velocity
    private float velocityX;
    private float velocityY;


    // constructor
    public Ribcage(BoxGame g, float x, float y, float velocityX, float velocityY){
        game = g;
        startingX = x;
        startingY = y;
        batch = game.batch;

        this.velocityX = velocityX;
        this.velocityY = velocityY;

        boneTexture = new Texture("ribcage.png");

        scaleRandom = game.randomWithRange(75, 100)*0.01f;

        width = boneTexture.getWidth()  / 100f * scaleRandom;
        height = boneTexture.getHeight()   / 100f * scaleRandom;


        create();
    }

    private void create(){

        // definition
        myBodyDef = new BodyDef();
        myBodyDef.fixedRotation = false;
        myBodyDef.type = BodyDef.BodyType.DynamicBody;
        myBodyDef.position.set(startingX, startingY);

        // *** 2) CREATE THE BODY *** /
        normalBoneBody = game.world.createBody(myBodyDef);

        // *** 3) FIXTURE FOR THE BODY *** /

        //  main
        groundBox = new PolygonShape();
        groundBox.setAsBox(width, height);

        width = width * 2;
        height = height * 2;

        FixtureDef playerFixtureDef = new FixtureDef();
        playerFixtureDef.density = 5;
        playerFixtureDef.restitution = 0.0f;
        playerFixtureDef.friction = 0.5f;
        playerFixtureDef.filter.groupIndex = game.GROUP_BONE;

        playerFixtureDef.shape = groundBox;
        Fixture playerBodyFixture = normalBoneBody.createFixture(playerFixtureDef);
        playerBodyFixture.setUserData("ribcage");

        float help = game.randomWithRange(0, 628) * 0.01f;
        normalBoneBody.setTransform(startingX,startingY, help);

        normalBoneBody.createFixture(playerFixtureDef);
        normalBoneBody.setUserData(this);

        normalBoneBody.setGravityScale(0.5f);

        setVelocity();
    }

    public  void dispose(){
        boneTexture.dispose();
    }

    public void drawBone(){
        batch.draw(boneTexture,


                normalBoneBody.getPosition().x - width / 2,
                normalBoneBody.getPosition().y - height / 2,
                width / 2  ,                  // originX
                height / 2,                   // originY
                width,                        // width
                height,                       // height
                1.0f,                         // scaleX
                1.0f,                         // scaleY
                normalBoneBody.getTransform().getRotation() * MathUtils.radiansToDegrees,
                0,          // Start drawing from x = 0
                0,                                                                      // Start drawing from y = 0
                boneTexture.getWidth(),                          // End drawing x
                boneTexture.getHeight(),                                  // End drawing y
                false,                                                                  // flipX
                false);                                                                 // flipY
    }

    public void explode(){
        System.out.println("Ribcage exploding");

        Vector2 temp = new Vector2();

        temp = normalBoneBody.getPosition();

        float helpX = temp.x;
        float helpY = temp.y + (height /2);

        for(int n = 0; n < 3; n++){
            game.getBoneList().add(new BoneDataHandler(helpX,helpY,1, true, n));
        }

    }
    private void setVelocity(){
        normalBoneBody.setLinearVelocity(velocityX,velocityY);
    }
    public Body getBoneBody(){
        return normalBoneBody;
    }
    public void collided(){
        colldided = true;
    }
    public boolean getCollidedStatus(){
        return colldided;
    }
}
