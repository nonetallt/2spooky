package tamk.fi.boxGame;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

/**
 * Created by Natura on 4.3.2015.
 */
public class CrawlingHand extends Bone {

    // utility
    private BoxGame game;
    private SpriteBatch batch;


    // starting
    private float startingX;
    private float startingY;

    // texture
    private Texture boneTexture;

    // body
    private BodyDef myBodyDef;
    private Body normalBoneBody;
    private PolygonShape groundBox;

    private float width;
    private float height;

    // collided statuses
    private boolean colldided = false;

    // crawling
    private final float speed = 0.8f;
    private boolean onGround = false;
    private boolean directionRight = false;
    private boolean directionSet = false;

    // velocity
    private float velocityX;
    private float velocityY;


    // constructor
    public CrawlingHand(BoxGame g, float x, float y, float velocityX, float velocityY){
        game = g;
        startingX = x;
        startingY = y;
        batch = game.batch;

        this.velocityX = velocityX;
        this.velocityY = velocityY;

        boneTexture = new Texture("skull.png");

        width = boneTexture.getWidth()  / 100f;
        height = boneTexture.getHeight()   / 100f;


        create();
    }

    private void create(){

        // definition
        myBodyDef = new BodyDef();
        myBodyDef.fixedRotation = true;
        myBodyDef.type = BodyDef.BodyType.DynamicBody;
        myBodyDef.position.set(startingX, startingY);

        // *** 2) CREATE THE BODY *** /
        normalBoneBody = game.world.createBody(myBodyDef);

        // *** 3) FIXTURE FOR THE BODY *** /

        //  main
        groundBox = new PolygonShape();
        groundBox.setAsBox(width, height);

        width = width * 2;
        height = height * 2;

        FixtureDef playerFixtureDef = new FixtureDef();
        playerFixtureDef.density = 5;
        playerFixtureDef.restitution = 0.0f;
        playerFixtureDef.friction = 0.5f;
        playerFixtureDef.filter.groupIndex = game.GROUP_BONE;


        playerFixtureDef.shape = groundBox;
        Fixture playerBodyFixture = normalBoneBody.createFixture(playerFixtureDef);
        playerBodyFixture.setUserData("crawlingHand");

        normalBoneBody.createFixture(playerFixtureDef);
        normalBoneBody.setUserData(this);

        normalBoneBody.setGravityScale(0.5f);

        setVelocity();
    }

    private void setVelocity(){
        normalBoneBody.setLinearVelocity(velocityX,velocityY);
    }
    public  void dispose(){
        boneTexture.dispose();
    }
    public void drawBone(){
        batch.draw(boneTexture,


                normalBoneBody.getPosition().x - width / 2,
                normalBoneBody.getPosition().y - height / 2,
                width / 2  ,                  // originX
                height / 2,                   // originY
                width,                        // width
                height,                       // height
                1.0f,                         // scaleX
                1.0f,                         // scaleY
                normalBoneBody.getTransform().getRotation() * MathUtils.radiansToDegrees,
                0,          // Start drawing from x = 0
                0,                                                                      // Start drawing from y = 0
                boneTexture.getWidth(),                          // End drawing x
                boneTexture.getHeight(),                                  // End drawing y
                false,                                                                  // flipX
                false);                                                                 // flipY
    }
    public Body getBoneBody(){
        return normalBoneBody;
    }
    public void collided(){
        colldided = true;
    }
    public boolean getCollidedStatus(){
        return colldided;
    }
    public void crawl(){

        Vector2 help = normalBoneBody.getPosition();
        Vector2 playerHelp = game.getPlayerPosition();

        if (onGround) {

            if(directionSet){

                if(directionRight){
                    normalBoneBody.applyLinearImpulse(new Vector2(+speed, 0f),
                            normalBoneBody.getWorldCenter(),
                            true);
                }
                else{
                    normalBoneBody.applyLinearImpulse(new Vector2(-speed, 0f),
                            normalBoneBody.getWorldCenter(),
                            true);
                }
            }
            else {

                if(help.x < playerHelp.x){
                    directionRight = true;
                    normalBoneBody.applyLinearImpulse(new Vector2(+speed, 0f),
                            normalBoneBody.getWorldCenter(),
                            true);
                }

                else if(help.x >= playerHelp.x){
                    directionRight = false;
                    normalBoneBody.applyLinearImpulse(new Vector2(-speed, 0f),
                            normalBoneBody.getWorldCenter(),
                            true);
                }
                directionSet = true;
            }
        }


    }
    public boolean isOnGround() {
        return onGround;
    }
    public void setOnGround(boolean onGround) {
        this.onGround = onGround;
    }

}
