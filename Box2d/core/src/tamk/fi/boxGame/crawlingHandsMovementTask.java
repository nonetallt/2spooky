package tamk.fi.boxGame;

import com.badlogic.gdx.utils.Timer;

/**
 * Created by Natura on 4.3.2015.
 */
public class crawlingHandsMovementTask extends Timer.Task {

    private BoxGame game;

    public crawlingHandsMovementTask(BoxGame g){
        game = g;
    }
    public void run(){
        game.crawlingHandTask();
    }
}
