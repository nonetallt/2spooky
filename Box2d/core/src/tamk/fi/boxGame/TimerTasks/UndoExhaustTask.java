package tamk.fi.boxGame.TimerTasks;

import com.badlogic.gdx.utils.Timer;
import tamk.fi.boxGame.Player;

/**
 * Created by Admin on 3/22/2016.
 */
public class UndoExhaustTask extends Timer.Task{

    private Player player;

    public UndoExhaustTask(Player player){
        this.player = player;
        player.setExhausted(true);
    }

    @Override public void run(){
        player.setExhausted(false);
    }
}
