package tamk.fi.boxGame.TimerTasks;

import com.badlogic.gdx.utils.Timer;
import tamk.fi.boxGame.Player;
import tamk.fi.boxGame.Settings.Static;

/**
 * Created by Admin on 3/22/2016.
 */
public class GhostPowerTask extends Timer.Task{


    private Player player;
    private Timer exhaustTimer;

    public GhostPowerTask(Player player){
        this.player = player;
        exhaustTimer = new Timer();
        player.setGhosting(true);
    }

    public void run(){
        player.setGhosting(false);
        exhaustTimer.scheduleTask(new UndoExhaustTask(player), Static.exhaustDuration);
    }

}
